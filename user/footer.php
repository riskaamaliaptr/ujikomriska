    <footer>
        <div class="">
            <p class="pull-right">Smk Negeri 1 Ciomas 
                <span class="lead"> <i class="fa fa-home"></i> Inventaris Sekolah!</span>
            </p>
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
</div>
    <!-- /page content -->
            <!-- chart js -->
            <script src="js/chartjs/chart.min.js"></script>
            <!-- bootstrap progress js -->
            <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
            <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
            <!-- icheck -->
            <script src="js/icheck/icheck.min.js"></script>
            <!-- tags -->
            <script src="js/tags/jquery.tagsinput.min.js"></script>
            <!-- switchery -->
            <script src="js/switchery/switchery.min.js"></script>
            <!-- daterangepicker -->
            <script type="text/javascript" src="js/moment.min2.js"></script>
            <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
            <!-- richtext editor -->
            <script src="js/editor/bootstrap-wysiwyg.js"></script>
            <script src="js/editor/external/jquery.hotkeys.js"></script>
            <script src="js/editor/external/google-code-prettify/prettify.js"></script>
            <!-- select2 -->
            <script src="js/select/select2.full.js"></script>
            <!-- form validation -->
            <script type="text/javascript" src="js/parsley/parsley.min.js"></script>
            <!-- textarea resize -->
            <script src="js/textarea/autosize.min.js"></script>
            <script>
                autosize($('.resizable_textarea'));
            </script>
            <!-- Autocomplete -->
            <script type="text/javascript" src="js/autocomplete/countries.js"></script>
            <script src="js/autocomplete/jquery.autocomplete.js"></script>
            <script type="text/javascript" src="js/jquery.min.js"></script>  
            <script src="js/custom.js"></script>
            <script src="js/bootstrap.min.js"></script>