DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlah` char(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `kondisi` varchar(30) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` datetime NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` char(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  PRIMARY KEY (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("9","laptop","Baik","ada","27","5","2019-08-03 00:00:00","4","V0001","1");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` char(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("5","laptop","123","ada");
INSERT INTO jenis VALUES("8","buku","202","rusak");
INSERT INTO jenis VALUES("10","baju","123","ada");
INSERT INTO jenis VALUES("11","bola","k01","ada");
INSERT INTO jenis VALUES("12","buku","k002","ada");
INSERT INTO jenis VALUES("13","Laptop","K004","ada");
INSERT INTO jenis VALUES("14","buku","001","rusak");
INSERT INTO jenis VALUES("15","bola","K02","ada");
INSERT INTO jenis VALUES("16","buku","K05","ada");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(30) NOT NULL,
  `nip` char(16) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(30) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varchar(30) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  `baned` enum('Yes','No') NOT NULL,
  `logintime` int(2) NOT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin","riskaamaliap97@gmail.com","admin","riskaap","1","","0");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(30) NOT NULL,
  `kode_ruang` char(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("3","Lab rpl","111","ada");
INSERT INTO ruang VALUES("4","Lab RPL","K001","ada");
INSERT INTO ruang VALUES("5","Lab Rpl 1","K03","Ada");
INSERT INTO ruang VALUES("6","Lab Animasi","K001","Ada");
INSERT INTO ruang VALUES("7","Bengkel Tpl","K0002","ada");



