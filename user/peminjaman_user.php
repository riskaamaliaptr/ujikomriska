<?php
include '../koneksi.php';
include 'header.php';
?>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a class="site_title"><i class="fa fa-paw"></i> <span>Inventaris!</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/admin3.gif" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>    
                        </div>
                    </div>
                    <!-- /menu prile quick info -->
                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>&nbsp;&nbsp;&nbsp;User!</h3>
                            <ul class="nav side-menu">
                                <li><a href="inventaris_user.php"><i class="fa fa-bar-chart-o"></i>Data Inventaris</a></li>
                                <li><a href="peminjaman_user.php"><i class="fa fa-check"></i>Peminjaman</a></li>
                                <li><a href="logout.php"><i class="fa fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>        
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->
            
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="dashboard_graph">
                            <div class="row x_title">
                                <div class="col-md-6">
                                    <h3>Peminjaman <small>Smkn 1 Ciomas</small></h3>
                                </div>                              
                            </div>                            
                        </div>
                    </div>
                </div>
                <br />

                <div class="row">
                    <?php
                    include '../koneksi.php';
                    $pilih=mysqli_query($koneksi, "SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis");
                    while ($tampil=mysqli_fetch_array($pilih)) {
                        
                        ?>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2 >Barang</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <img src="../gambar_barang/<?=$tampil['gambar'];?>" style="height: 150px; width: 180px;">
                                  <h5><?=$tampil['nama_jenis'];?></h5>
                                  <h4><?=$tampil['nama'];?></h4>
                                  <span>Stock : <?=$tampil['jumlah'];?> unit</span><br><br>
                                  <!-- Button trigger modal -->
                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#pinjam<?=$tampil['id_inventaris'];?>">
                                      Pinjam
                                  </button>

                                  <!-- Modal -->
                                  <div class="modal fade" id="pinjam<?=$tampil['id_inventaris'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h3 class="modal-title" id="myModalLabel" style="text-align: center">Form-Peminjaman</h3>
                                        </div>
                                        <div class="modal-body">
                                            <form action="proses_pinjam_user.php" method="post">
                                                <input type="hidden" name="id_pegawai" value="<?php echo $_SESSION['pegawai'];?>">
                                                <input type="hidden" name="id_inventaris" value="<?=$tampil['id_inventaris'];?>">
                                                <div class="form-group">
                                                    <label>Nama Peminjam</label>
                                                    <input type="text" class="form-control" name="nama_pegawai" value="<?php echo $_SESSION['nama_pegawai'];?>" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nama Barang</label>
                                                    <input type="text" class="form-control" name="nama" value="<?php echo $tampil['nama'];?>" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Jumlah</label>
                                                    <input type="number" name="jumlah" max="<?php echo $tampil['jumlah'];?>" value="1">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                                <input type="submit" class="btn btn-primary" name="simpan" value="simpan">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>            
                    </div>  
                </div>   
                <?php
                }
                ?>  
        </div>

        <?php
        include 'footer.php';
        ?>
    </body>
    </html>