<?php
include '../koneksi.php';
include 'header.php';
?>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a class="site_title"><i class="fa fa-paw"></i> <span>Inventaris!</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/admin3.gif" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>    
                        </div>
                    </div>
                    <!-- /menu prile quick info -->
                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>&nbsp;&nbsp;&nbsp;User!</h3>
                            <ul class="nav side-menu">
                                <li><a href="inventaris_user.php"><i class="fa fa-bar-chart-o"></i>Data Inventaris</a></li>
                                <li><a href="peminjaman_user.php"><i class="fa fa-check"></i>Peminjaman</a></li>
                                <li><a href="logout.php"><i class="fa fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>        
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->
            
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="dashboard_graph">
                            <div class="row x_title">
                                <div class="col-md-6">
                                    <h3>Inventaris <small>Smkn 1 Ciomas</small></h3>
                                </div>                              
                            </div>                            
                        </div>
                    </div>
                </div>
                <br />

                <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2 >Rekayasa Perangkat Lunak</h2>  
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <a href="tbl_inventaris_user.php?jurusan=Rekayasa Perangkat Lunak"><img src="images/rpl.jpg" width="80%" height="80%" align="center"></a> 
                                </div>
                            </div>
                        </div>

                       <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2 >Animasi</h2>   
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <a href="tbl_inventaris_user.php?jurusan=Animasi"><img src="images/anm.jpg" width="80%" height="80%" align="center"></a>     
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2 >Teknik Kendaraan Ringan</h2> 
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <a href="tbl_inventaris_user.php?jurusan=Teknik Kendaraan Ringan"><img src="images/tkr.jpg" width="80%" height="80%" align="center"></a>   
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2 >Teknik Pengelasan</h2> 
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <a href="tbl_inventaris_user.php?jurusan=Teknik Pengelasan"><img src="images/tpl.png" width="80%" height="80%" align="center"></a>
                                </div>
                            </div>
                        </div>
                         <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2 >Broadcasting</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <a href="tbl_inventaris_user.php?jurusan=Broadcasting"><img src="images/bc.png" width="89%" height="89%" align="center" ></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2 >Ruang Tata Usaha</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <a href="tbl_inventaris_user.php?jurusan=Tata Usaha"><img src="images/tu.jpg" width=" 87%" height="89%" align="center" ></a>
                              </div>
                          </div>
                      </div>
                    </div>

                    <?php
                        include 'footer.php';
                    ?>
</body>
</html>