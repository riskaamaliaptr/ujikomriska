<?php
include '../koneksi.php';
include 'header.php';
?>
<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav class="" role="navigation">
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>         
    </nav>
  </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">
        <div class="row x_title">
          <div class="col-md-6">
            <h3>Data Inventaris <small>Smkn 1 Ciomas</small></h3>
          </div>                              
        </div>                            
      </div>
    </div>
  </div>
  <br />
  <!-- !!!!!!!Tabel!!!!!!!! -->
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_content">
        <div class="table-responsive">
          <table id="riska" class="table table-striped responsive-utilities jambo_table">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Kondisi</th>                  
                <th>Keterangan</th>               
                <th>Jumlah</th>
                <th>Nama Jenis</th>
                <th>Tanggal Register</th>
                <th>Nama Ruang</th>                  
                <th>Kode Inventaris</th>               
                <th>Nama Petugas</th>
                <th>Gambar</th>
              </tr>
            </thead>
            <tbody>
              <?php
              include '../koneksi.php';
              if (isset($_GET['jurusan'])) {
                $bebas = $_GET['jurusan'];
                $pilih = mysqli_query($koneksi, "SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis JOIN ruang r ON i.id_ruang=r.id_ruang JOIN petugas p ON i.id_petugas=p.id_petugas WHERE nama_jenis='$bebas'");
              }else{
                $pilih = mysqli_query($koneksi, "SELECT * FROM inventaris");
              }
              $no=1;
              while ($data = mysqli_fetch_array($pilih)) {          
                ?>
                <tr>                                
                  <td height="42"><?php echo $no++; ?></td>
                  <td><?php echo $data['nama']; ?></td>
                  <td><?php echo $data['kondisi']; ?></td>                    
                  <td><?php echo $data['keterangan']; ?></td>
                  <td><?php echo $data['jumlah']; ?></td>  
                  <td><?php echo $data['nama_jenis']; ?></td>   
                  <td><?php echo $data['tanggal_register']; ?></td>    
                  <td><?php echo $data['nama_ruang']; ?></td>   
                  <td><?php echo $data['kode_inventaris']; ?></td>    
                  <td><?php echo $data['username']; ?></td>     
                  <td><img src="../gambar_barang/<?=$data['gambar']; ?>" width="50px"></td>
                </tr>

                <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>   
  </div>

  <?php
  include 'footer.php';
  ?>
  <!-- Datatables -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="js/datatables/js/jquery.dataTables.min.js"></script>
  <script>
   $(document).ready(function() {
     $('#riska').DataTable();
   });
 </script>