<?php
include '../koneksi.php';
include 'header.php';
?>
<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>         
        </nav>
    </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                <div class="row x_title">
                    <div class="col-md-6">
                        <h3>Inventaris <small>Smkn 1 Ciomas</small></h3>
                    </div>                              
                </div>                            
            </div>
        </div>
    </div>
    <br />

    <div class="row">
       <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 >Rekayasa Perangkat Lunak</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <a href="tbl_inventaris.php?jurusan=Rekayasa Perangkat Lunak"><img src="images/rpl.jpg" width="80%" height="80%" align="center" ></a>
              
          </div>
      </div>
  </div>

  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 >Animasi</h2>  
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a href="tbl_inventaris.php?jurusan=Animasi"><img src="images/anm.jpg" width="80%" height="80%" align="center" ></a>
      </div>
  </div>
</div>

<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 >Teknik Kendaraan Ringan</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a href="tbl_inventaris.php?jurusan=Teknik Kendaraan Ringan"><img src="images/tkr.jpg" width="80%" height="80%" align="center" ></a>          
      </div>
  </div>
</div>

<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 >Teknik Pengelasan</h2>            
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a href="tbl_inventaris.php?jurusan=Teknik Pengelasan"><img src="images/tpl.png" width="80%" height="80%" align="center" >
          </a>
      </div>
  </div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 >Broadcasting</h2>           
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a href="tbl_inventaris.php?jurusan=Broadcasting"><img src="images/bc.png" width="89%" height="89%" align="center"></a>          
      </div>
  </div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 >Ruang Tata Usaha</h2>            
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
         <a href="tbl_inventaris.php?jurusan=Tata Usaha"><img src="images/tu.jpg" width="87%" height="89%" align="center"></a></div>
    </div>
</div>

</div>

<?php
include 'footer.php';
?>