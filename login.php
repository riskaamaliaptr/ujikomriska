<?php
session_start();
include 'koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Halaman Login!  </title>

    <!-- Bootstrap core CSS -->
    <link href="admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="admin/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="admin/css/animate.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="admin/css/custom.css" rel="stylesheet">
    <link href="admin/css/icheck/flat/green.css" rel="stylesheet">
    <script src="admin/js/jquery.min.js"></script>
<style>
    .login_content form div a {
    font-size: 12px;
    margin: -5px 7px 0px 0;
}
</style>
</head>

<body background="admin/images/1.jpg">   
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>
        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <form action="cek_login.php" method="post">
                        <h1>Login</h1>
                        <div>
                            <input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off"  />
                        </div>
                        <div>
                            <input type="password" class="form-control" name="password" placeholder="Password"  />
                        </div>
                        <div>                        
                            <input type="submit" name="simpan" value="login" class="btn btn-default" >
                            <a class="btn btn-default" href="login_user.php">Pegawai</a>
                            <a class="reset_pass" href="forgot-pass.php">Forgot Password?</a><br>
                            
                        </div>
                        <a class="help" href="help.php" style="margin: 61%;">Help?</a>
                        <div class="clearfix"></div>
                        <div class="separator">
                            <div>
                                <h1><i class="fa fa-home" style="font-size: 26px;"></i> Inventaris Sekolah</h1>
                                <p>©2019 Jl.Raya Laladon Ds.Laladon Kec.Ciomas Kab.Bogor  </p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>
</body>
</html>