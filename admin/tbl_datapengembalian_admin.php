<?php 
include '../koneksi.php';
include 'header.php';
?> 

<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav class="" role="navigation">
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>
    </nav>
  </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">
        <div class="row x_title">
          <div class="col-md-6">
            <h3>Data Pengembalian <small>Admin&Operator</small></h3>
          </div>                             
        </div>
      </div>
    </div>
  </div>
  <br />
  
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
     <!-- !!!!!!!!!!!!Tabel!!!!!!!!! -->
     <a href="export_excel_kembali_admin.php"> <button type="button" class="btn btn-primary">Export Excel</button></a>
     <a href="pdf_kembali_admin.php"> <button type="button" class="btn btn-primary">Export Pdf</button></a>
     <div class="x_content">
      <div class="table-responsive">
        <table id="riska" class="table table-striped responsive-utilities jambo_table">
          
          <thead>
            <tr class="headings">
              <th>No</th>
              <th>Nama Peminjam</th>
              <th>Nama Barang</th>                  
              <th>Tanggal Pinjam</th>
              <th>Tanggal Kembali</th>
              <th>Jumlah</th> 
              <th>Status Peminjaman</th>              
            </tr>
          </thead>
          <tbody>

            <?php
            $no=1;
            $select=mysqli_query($koneksi, "SELECT * from peminjaman p JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman JOIN petugas g ON P.id_petugas=g.id_petugas JOIN inventaris i ON d.id_inventaris=i.id_inventaris  WHERE status_peminjaman='Telah Dikembalikan'");
            while($data=mysqli_fetch_array($select)){
              ?>
              <tr>
                <td height="42"><?php echo $no++; ?></td>
                <td><?php echo $data['username']; ?></td>
                <td><?php echo $data['nama'];?></td>
                <td><?php echo $data['tanggal_pinjam'];?></td>  
                <td><?php echo $data['tanggal_kembali'];?></td>
                <td><?php echo $data['jumlah_pinjam'];?></td>  
                <td><?php echo $data['status_peminjaman'];?></td> 
                
              </tr>

              <?php
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php
include 'footer.php';
?>
<!-- Datatables -->
<script src="assets/js/jquery.min.js"></script>
<script src="js/datatables/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function() {
    $('#riska').DataTable();
  });
</script>