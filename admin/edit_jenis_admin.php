<?php
include '../koneksi.php';
include 'header.php';
$id_jenis=$_GET['id_jenis'] ;
$nama= mysqli_query($koneksi, "SELECT * FROM jenis WHERE id_jenis='$id_jenis'");
$r = mysqli_fetch_array($nama);

?>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>         
        </nav>
    </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                <div class="row x_title">
                    <div class="col-md-6">
                        <h3>Edit Jenis Barang <small>Smkn 1 Ciomas</small></h3>
                    </div>                              
                </div>                            
            </div>
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <form action="" method="POST" class="form-horizontal form-label-left" novalidate>     
                        <span class="section">Jenis Barang</span>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_jenis">Nama Jenis <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="nama_jenis" name="nama_jenis" class="form-control col-md-7 col-xs-12" placeholder="nama_jenis" autocomplete="off" required="required" value="<?=$r['nama_jenis'];?>" readonly>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode_jenis">Kode Jenis <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="kode_jenis" name="kode_jenis" class="form-control col-md-7 col-xs-12" placeholder="kode_jenis" autocomplete="off" required="required" value="<?=$r['kode_jenis'];?>" readonly>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12" placeholder="keterangan" autocomplete="off" required="required"  value="<?=$r['keterangan'];?>">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="submit" name="submit" class="btn btn-info" value="Simpan" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
    include 'footer.php';
    ?>
    
    <?php
    include '../koneksi.php';
    if(isset($_POST['submit']))
    {
        $nama_jenis = $_POST['nama_jenis'];
        $kode_jenis = $_POST['kode_jenis'];
        $keterangan = $_POST['keterangan'];

        $edit = mysqli_query($koneksi, "UPDATE jenis SET nama_jenis='$nama_jenis',kode_jenis='$kode_jenis',keterangan='$keterangan' WHERE id_jenis='$_GET[id_jenis]'");
        if($edit){
           /* echo "<script>window.location.assign('jenis_admin.php')</script>";*/
           echo "<script>
           window.alert('Data Berhasil Di Edit')
           window.location.assign('jenis_admin.php')
           </script>";
       }else{
          echo"GAGAL";
      }
  }
  ?>