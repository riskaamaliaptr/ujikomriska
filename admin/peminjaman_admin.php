<?php
include '../koneksi.php';
include 'header.php';
?>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>         
        </nav>
    </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                <div class="row x_title">
                    <div class="col-md-6">
                        <h3>Peminjaman <small>Smkn 1 Ciomas</small></h3>
                    </div>                              
                </div>                            
            </div>
        </div>
    </div>
    <br />

    <div class="row">
        <?php
        include '../koneksi.php';
        $pilih=mysqli_query($koneksi, "SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis");
        while ($tampil=mysqli_fetch_array($pilih)) {
            
            ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 >Barang</h2>
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <img src="../gambar_barang/<?=$tampil['gambar'];?>" style="height: 150px; width: 180px;" >
                      <h5><?=$tampil['nama_jenis'];?></h5>
                      <h4><?=$tampil['nama'];?></h4>           
                      <span>Stock : <?=$tampil['jumlah'];?> unit</span><br><br>
                      <!-- Button trigger modal -->
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#pinjam<?=$tampil['id_inventaris'];?>">
                          Pinjam
                      </button>

                      <!-- Modal -->
                      <div class="modal fade" id="pinjam<?=$tampil['id_inventaris'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title" id="myModalLabel" style="text-align: center">Form-Peminjaman</h3>
                            </div>
                            <div class="modal-body">
                                <form action="pro_pinjam_admin.php" method="post">
                                    <input type="hidden" name="id_petugas" value="<?php echo $_SESSION['petugas'];?>">
                                    <input type="hidden" name="id_inventaris" value="<?=$tampil['id_inventaris'];?>">
                                    <div class="form-group">
                                        <label>Nama Peminjam</label>
                                        <input type="text" class="form-control" name="username" value="<?php echo $_SESSION['username'];?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Barang</label>
                                        <input type="text" class="form-control" name="nama" value="<?php echo $tampil['nama'];?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Jumlah</label>
                                        <input type="number" name="jumlah" max="<?php echo $tampil['jumlah'];?>" value="1">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                    <input type="submit" class="btn btn-primary" name="simpan" value="simpan">        
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>            
        </div>  
    </div>   
    <?php
    }
    ?>  
</div>

<?php
include 'footer.php';
?>