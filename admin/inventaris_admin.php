<?php
include '../koneksi.php';
include 'header.php';
?>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
        </nav>
    </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                <div class="row x_title">
                    <div class="col-md-6">
                        <h3>Data Inventaris <small>Smkn 1 Ciomas</small></h3>
                    </div>                          
                </div>    
            </div>
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 >Rekayasa Perangkat Lunak</h2>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <a href="inventaris.php?jurusan=Rekayasa Perangkat Lunak"><img src="images/rpl.jpg" style="width: 80%; height: 80%"></a>
                  
              </div>
          </div>
      </div>

      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2 >Animasi</h2>
                
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <a href="inventaris.php?jurusan=Animasi"><img src="images/anm.jpg" style="width: 80%; height: 80%"></a>
              
          </div>
      </div>
  </div>

  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 >Teknik Kendaraan Ringan</h2>
            
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a href="inventaris.php?jurusan=Teknik Kendaraan Ringan"><img src="images/tkr.jpg" style="width: 80%; height: 80%"></a>
          
      </div>
  </div>
</div>

<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 >Teknik Pengelasan</h2>
            
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a href="inventaris.php?jurusan=Teknik Pengelasan"><img src="images/tpl.png" style="width: 72%; height: 80%" >
          </a>
      </div>
  </div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 >Broadcasting</h2>
            
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a href="inventaris.php?jurusan=Broadcasting"><img src="images/bc.png" style="width: 80%; height: 80%" ></a>
          
      </div>
  </div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2 >Ruang Tata Usaha</h2>
            
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a href="inventaris.php?jurusan=Tata Usaha"><img src="images/tu.jpg" style="width: 79%; height: 80%"></a>
          
      </div>
  </div>
</div>
</div>

<?php
include 'footer.php';
?>
<!-- Datatables -->
<script src="assets/js/jquery.min.js"></script>
<script src="js/datatables/js/jquery.dataTables.min.js"></script>
<script>
 $(document).ready(function() {
     $('#riska').DataTable();
 });
</script>