<?php
include '../koneksi.php';
include 'header.php';
$id_petugas=$_GET['id_petugas'] ;
$nama= mysqli_query($koneksi, "SELECT * FROM petugas WHERE id_petugas='$id_petugas'");
$r = mysqli_fetch_array($nama);

?>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>         
        </nav>
    </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                <div class="row x_title">
                    <div class="col-md-6">
                        <h3>Edit Data Petugas <small>Smkn 1 Ciomas</small></h3>
                    </div>                              
                </div>                            
            </div>
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <form action="" method="POST" class="form-horizontal form-label-left" novalidate>     
                        <span class="section">Data Petugas</span>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="username" name="username" class="form-control col-md-7 col-xs-12" placeholder="username" autocomplete="off" required="required" value="<?=$r['username'];?>" readonly>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="email" name="email" class="form-control col-md-7 col-xs-12" placeholder="email" autocomplete="off" required="required" value="<?=$r['email'];?>" readonly>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_petugas">Nama Petugas<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="nama_petugas" name="nama_petugas" class="form-control col-md-7 col-xs-12" placeholder="nama_petugas" autocomplete="off" required="required"  value="<?=$r['nama_petugas'];?>">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="submit" name="submit" class="btn btn-info" value="Simpan" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
    include 'footer.php';
    ?>
    
    <?php
    include '../koneksi.php';
    if(isset($_POST['submit']))
    {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $nama_petugas = $_POST['nama_petugas'];

        $edit = mysqli_query($koneksi, "UPDATE petugas SET username='$username',email='$email',nama_petugas='$nama_petugas' WHERE id_petugas='$_GET[id_petugas]'");
        if($edit){
           /* echo "<script>window.location.assign('jenis_admin.php')</script>";*/
           echo "<script>
           window.alert('Data Berhasil Di Edit')
           window.location.assign('datapetugas_admin.php')
           </script>";
       }else{
          echo"GAGAL";
      }
  }
  ?>