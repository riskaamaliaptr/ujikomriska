<?php
include '../koneksi.php';
include 'header.php';
?>

<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav class="" role="navigation">
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>         
    </nav>
  </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">
        <div class="row x_title">
          <div class="col-md-6">
            <h3>Data Inventaris <small>Smkn 1 Ciomas</small></h3>
          </div>                              
        </div>                            
      </div>
    </div>
  </div>
  <br />
  <!-- modal -->
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">  
      <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Tambah Inventaris
      </button>
      <a href="export_excel_inventaris.php"> <button type="button" class="btn btn-primary">Export Excel</button></a>
      <a href="pdf_inventaris.php"> <button type="button" class="btn btn-primary">Export Pdf</button></a>

      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
             <h4 class="modal-title" id="myModalLabel" align="center">Tambah Inventaris</h4>
           </div>
           <div class="modal-body">
             <form method="post"  action="pro_simpan_inventaris_admin.php" class="form-group">
               <div class="container">
                <div class="col-sm-offset-1 col-sm-10">
                  <div class="form-group">
                    <label>Nama Barang</label>
                    <td><input type="text"  class="form-control"  name="nama" autocomplete="off" required></td>
                  </div>
                  <label>Kondisi</label><br>
                  <select class="form-control" name="kondisi">
                    <option disabled selected>Pilih Kondisi</option>
                    <option>Baik</option>
                    <option>Rusak</option>
                    <option>Rusak Ringan</option>
                  </select><br>
                  <div class="form-group">
                    <label>Keterangan</label>
                    <td><input type="text"  class="form-control"  name="keterangan" autocomplete="off" required></td>
                  </div>
                  <div class="form-group">
                    <label>Jumlah</label>
                    <td><input type="number"  class="form-control"  name="jumlah" autocomplete="off" required></td>
                  </div>
                  <div class="form-group">
                    <label for="id_jenis">Nama Jenis</label>
                    <select class="form-control" name="id_jenis">
                      <option value="" disabled selected>Pilih Nama Jenis</option>
                      <?php
                      include 'koneksi.php';
                      $query = mysqli_query($koneksi, "SELECT * FROM jenis");
                      while($data=mysqli_fetch_array($query)) {
                        ?>
                        <option value="<?php echo $data['id_jenis'];?>"><?php echo $data['nama_jenis'];?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Tanggal Register</label>
                      <td><input type="date"  class="form-control"  name="tanggal_register" autocomplete="off" required></td>
                    </div>
                    <div class="form-group">
                      <label for="id_ruang">Nama Ruang</label>
                      <select class="form-control" name="id_ruang">
                        <option value="" disabled selected>Pilih Ruang</option>
                        <?php
                        include 'koneksi.php';
                        $select = mysqli_query($koneksi, "SELECT * FROM ruang");
                        while($isi=mysqli_fetch_array($select)) {
                          ?>
                          <option value="<?php echo $isi['id_ruang'];?>"><?php echo $isi['nama_ruang'];?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Kode Inventaris</label>
                        <?php
                        $koneksi = mysqli_connect("localhost","root","","ujikom");
                        $cari_kd = mysqli_query($koneksi, "SELECT max(kode_inventaris) as kode from inventaris");
                                        // besar / kode yang masuk
                        $tm_cari = mysqli_fetch_array($cari_kd);
                        $kode = substr($tm_cari['kode'],1,4);
                        $tambah = $kode+1;
                        if ($tambah<10) {
                          $kode_inventaris = "V000".$tambah;
                        }else{
                          $kode_inventaris = "V00".$tambah;
                        }
                        ?>
                        <input class="form-control" name="kode_inventaris" value="<?php echo $kode_inventaris; ?>" type="text" placeholder="Masukan Kode Inventaris" required readonly>
                      </div>

                                  <!--  <div class="form-group">
                                        <label for="id_petugas">Petugas</label>
                                        <input type="text" class="form-control" value="<?php echo $_SESSION['username'] ?>" readonly>
                                        <input type="hidden" class="form-control" value="<?php echo $_SESSION['id_pengguna'] ?>" name="id_petugas" readonly>
                                      </div>  -->  
                                      <div class="form-group">
                                        <label for="id_petugas">Petugas</label>
                                        <select class="form-control" name="id_petugas" readonly>
                                         
                                          <?php
                                          include '../koneksi.php';
                                          $select = mysqli_query($koneksi, "SELECT * FROM petugas");
                                          while($isi=mysqli_fetch_array($select)) {
                                            ?>
                                            <option value="<?php echo $isi['id_petugas'];?>"><?php echo $isi['username'];?></option>
                                            <?php } ?>
                                          </select>
                                        </div>

                                        <div class="form-group">
                                          <label for="exampleInputFile">File input</label>
                                          <input type="file" name="gambar" id="exampleInputFile" placeholder="gambar">
                                          <p class="help-block">Masukan Gambar Inventaris.</p>
                                        </div>
                                      </div>
                                    </div>                           
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                    <input type="submit" class="btn btn-primary" name="simpan" value="simpan">
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- modal -->
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="x_content">
                            <div class="table-responsive">
                              <table id="riska" class="table table-striped responsive-utilities jambo_table">
                                <thead>
                                  <tr>
                                    <th>No</th>
                                    <th>Nama Barang</th>
                                    <th>Kondisi</th>                  
                                    <th>Keterangan</th>               
                                    <th>Jumlah</th>
                                    <th>Nama Jenis</th>
                                    <th>Tanggal Register</th>
                                    <th>Nama Ruang</th>                  
                                    <th>Kode Inventaris</th>               
                                    <th>Nama Petugas</th>
                                    <th>Gambar</th>
                                    <th>Option</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  include '../koneksi.php';
                                  if (isset($_GET['jurusan'])) {
                                    $bebas = $_GET['jurusan'];
                                    $pilih = mysqli_query($koneksi, "SELECT * FROM inventaris i JOIN jenis j ON i.id_jenis=j.id_jenis JOIN ruang r ON i.id_ruang=r.id_ruang JOIN petugas p ON i.id_petugas=p.id_petugas WHERE nama_jenis='$bebas'");
                                  }else{
                                    $pilih = mysqli_query($koneksi, "SELECT * FROM inventaris");
                                  }
                                  $no=1;
                                  while ($data = mysqli_fetch_array($pilih)) {          
                                    ?>
                                    <tr>                                
                                      <td height="42"><?php echo $no++; ?></td>
                                      <td><?php echo $data['nama']; ?></td>
                                      <td><?php echo $data['kondisi']; ?></td>                    
                                      <td><?php echo $data['keterangan']; ?></td>
                                      <td><?php echo $data['jumlah']; ?></td>  
                                      <td><?php echo $data['nama_jenis']; ?></td>   
                                      <td><?php echo $data['tanggal_register']; ?></td>    
                                      <td><?php echo $data['nama_ruang']; ?></td>   
                                      <td><?php echo $data['kode_inventaris']; ?></td>    
                                      <td><?php echo $data['username']; ?></td>
                                      <td><img src="../gambar_barang/<?=$data['gambar']; ?>" width="50px"></td>       
                                      <td>
                                        <a href='hapus_inventaris_admin.php?id_inventaris=<?=$data['id_inventaris'];?>'><button type="button" class="btn btn-primary" data-placement="left" title="Hapus Inventaris">
                                          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button></a>
                                  
                                    </td>
                                  </tr>

                                  <?php
                                }
                                ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>                     
                    </div>

                    <?php
                    include 'footer.php';
                    ?>
                    <!-- Datatables -->
                    <script src="assets/js/jquery.min.js"></script>
                    <script src="js/datatables/js/jquery.dataTables.min.js"></script>
                    <script>
                     $(document).ready(function() {
                       $('#riska').DataTable();
                     });
                   </script>