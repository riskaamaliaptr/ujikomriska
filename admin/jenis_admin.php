<?php 
include '../koneksi.php';
include 'header.php';
?> 

<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav class="" role="navigation">
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>
    </nav>
  </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">
        <div class="row x_title">
          <div class="col-md-6">
            <h3>Data Jenis <small>Smkn 1 Ciomas</small></h3>
          </div>                             
        </div>
      </div>
    </div>
  </div>
  <br />

  <!-- modal -->
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">  
      <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Tambah Jenis
      </button>
      <a href="export_excel_jenis.php"> <button type="button" class="btn btn-primary">Export Excel</button></a>
      <a href="pdf_jenis.php"> <button type="button" class="btn btn-primary">Export Pdf</button></a>

      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
             <h4 class="modal-title" id="myModalLabel" align="center">Tambah Jenis Barang</h4>
           </div>
           <div class="modal-body">
             <form method="post"  action="pro_simpan_jenis_admin.php" class="form-group">
               <div class="container">
                <div class="col-sm-offset-1 col-sm-10">
                  <div class="form-group">
                    <label>Nama Jenis</label>
                    <td><input type="text"  class="form-control"  name="nama_jenis" placeholder="Masukan Nama Jenis" autocomplete="off" required></td>
                  </div>
                  <div class="form-group">
                    <label>Kode Jenis</label>
                    <?php
                    $koneksi = mysqli_connect("localhost","root","","ujikom");
                    $cari_kd = mysqli_query($koneksi, "SELECT max(kode_jenis) as kode from jenis");
                                        // besar / kode yang masuk
                    $tm_cari = mysqli_fetch_array($cari_kd);
                    $kode = substr($tm_cari['kode'],1,4);
                    $tambah = $kode+1;
                    if ($tambah<10) {
                      $kode_jenis = "K000".$tambah;
                    }else{
                      $kode_jenis = "K00".$tambah;
                    }
                    ?>
                    <input class="form-control" name="kode_jenis" value="<?php echo $kode_jenis; ?>" type="text" placeholder="Masukan Kode Jenis" required readonly>
                  </div>
                  <div class="form-group">
                    <label>Keterangan</label>
                    <td><input type="text"  class="form-control"  name="keterangan" placeholder="Masukan Keterangan" autocomplete="off" required=""></td>
                  </div>
                </div>
              </div>                             
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary" name="simpan" value="simpan">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- modal -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
   <!-- !!!!!!!!!!!!Tabel!!!!!!!!! -->
   <div class="x_content">
    <div class="table-responsive">
      <table id="riska" class="table table-striped responsive-utilities jambo_table">             
        <thead>
          <tr class="headings">
            <th>No</th>
            <th>nama Jenis</th>
            <th>Kode Jenis</th>                  
            <th>keterangan</th>               
            <th>option</th>
          </tr>
        </thead>
        <tbody>

          <?php
          $no=1;
          $select=mysqli_query($koneksi, "SELECT * from jenis ");
          while($data=mysqli_fetch_array($select)){
            ?>
            <tr>
              <td height="42"><?php echo $no++; ?></td>
              <td><?php echo $data['nama_jenis']; ?></td>
              <td><?php echo $data['kode_jenis'] ?></td>
              <td><?php echo $data['keterangan']?></td>                       
              <td>
                <a class="edit" href="edit_jenis_admin.php?id_jenis=<?php echo $data['id_jenis']; ?>">
                  <button type="button" class="btn btn-success" data-placement="left" title="Edit Barang">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                </button></a>
                <a class="hapus" href="hapus_jenis_admin.php?id_jenis=<?php echo $data['id_jenis']; ?>">                  <button type="button" class="btn btn-primary" data-placement="left" title="Hapus Barang">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button></a>                     
              </td>          
            </tr>

            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
<?php
include 'footer.php';
?>
<!-- Datatables -->
<script src="assets/js/jquery.min.js"></script>
<script src="js/datatables/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function() {
    $('#riska').DataTable();
  });
</script>