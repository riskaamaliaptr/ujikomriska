<?php
include '../koneksi.php';
include 'header.php';
$id_pegawai=$_GET['id_pegawai'] ;
$nama= mysqli_query($koneksi, "SELECT * FROM pegawai WHERE id_pegawai='$id_pegawai'");
$r = mysqli_fetch_array($nama);

?>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>         
        </nav>
    </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                <div class="row x_title">
                    <div class="col-md-6">
                        <h3>Edit Data Pegawai <small>Smkn 1 Ciomas</small></h3>
                    </div>                              
                </div>                            
            </div>
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <form action="" method="POST" class="form-horizontal form-label-left" novalidate>     
                        <span class="section">Data Pegawai</span>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_pegawai">Nama Pegawai <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="nama_pegawai" name="nama_pegawai" class="form-control col-md-7 col-xs-12" placeholder="nama_pegawai" autocomplete="off" required="required" value="<?=$r['nama_pegawai'];?>" readonly>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nip">Nip <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="nip" name="nip" class="form-control col-md-7 col-xs-12" placeholder="nip" autocomplete="off" required="required" value="<?=$r['nip'];?>" readonly>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="alamat" name="alamat" class="form-control col-md-7 col-xs-12" placeholder="alamat" autocomplete="off" required="required"  value="<?=$r['alamat'];?>">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telp">No Telepon<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="no_telp" name="no_telp" class="form-control col-md-7 col-xs-12" placeholder="no_telp" autocomplete="off" required="required"  value="<?=$r['no_telp'];?>">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="submit" name="submit" class="btn btn-info" value="Simpan" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
    include 'footer.php';
    ?>
    
    <?php
    include '../koneksi.php';
    if(isset($_POST['submit']))
    {
        $nama_pegawai = $_POST['nama_pegawai'];
        $nip = $_POST['nip'];
        $alamat = $_POST['alamat'];
        $no_telp = $_POST['no_telp'];


        $edit = mysqli_query($koneksi, "UPDATE pegawai SET nama_pegawai='$nama_pegawai',nip='$nip',alamat='$alamat',no_telp='$no_telp' WHERE id_pegawai='$_GET[id_pegawai]'");
        if($edit){
           /* echo "<script>window.location.assign('jenis_admin.php')</script>";*/
           echo "<script>
           window.alert('Data Berhasil Di Edit')
           window.location.assign('datapegawai_admin.php')
           </script>";
       }else{
          echo"GAGAL";
      }
  }
  ?>