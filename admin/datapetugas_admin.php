<?php 
include '../koneksi.php';
include 'header.php';
?> 

<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav class="" role="navigation">
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>
    </nav>
  </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">
        <div class="row x_title">
          <div class="col-md-6">
            <h3>Data Petugas <small>Smkn 1 Ciomas</small></h3>
          </div>                             
        </div>
      </div>
    </div>
  </div>
  <br />

  <!-- modal -->
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">  
      <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Tambah Petugas
      </button>
      <a href="export_excel_petugas.php"> <button type="button" class="btn btn-primary">Export Excel</button></a>
      <a href="pdf_petugas.php"> <button type="button" class="btn btn-primary">Export Pdf</button></a>

      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
             <h4 class="modal-title" id="myModalLabel" align="center">Tambah Petugas</h4>
           </div>
           <div class="modal-body">
             <form method="post"  action="pro_simpan_petugas_admin.php" class="form-group">
               <div class="container">
                <div class="col-sm-offset-1 col-sm-10">
                  <div class="form-group">
                    <label>Username</label>
                    <td><input type="text"  class="form-control"  name="username" placeholder="Masukan Username Anda" autocomplete="off" required></td>
                  </div>
                  <div class="form-group">
                    <label>Email</label>
                    <td><input type="text"  class="form-control"  name="email" placeholder="Masukan Email Anda" autocomplete="off" required></td>
                  </div>
                  <div class="form-group">
                    <label>Password</label>
                    <td><input type="text"  class="form-control"  name="password" placeholder="Masukan Password" autocomplete="off" required=""></td>
                  </div>
                  <div class="form-group">
                    <label>Nama Petugas</label>
                    <td><input type="text"  class="form-control"  name="nama_petugas" placeholder="Masukan Nama Petugas" autocomplete="off" required=""></td>
                  </div>
                  <div class="form-group">
                    <label>Id Level</label>
                    <td><input type="number"  class="form-control"  name="id_level" placeholder="Id Level" autocomplete="off" required=""></td>
                  </div>
                </div>
              </div>                             
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary" name="simpan" value="simpan">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- modal -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
   <!-- !!!!!!!!!!!!Tabel!!!!!!!!! -->
   <div class="x_content">
    <div class="table-responsive">
      <table id="riska" class="table table-striped responsive-utilities jambo_table">                 
        <thead>
          <tr class="headings">
            <th>No</th>
            <th>Username</th>
            <th>Email</th>                  
            <th>Nama Petugas</th> 
            <th>Option</th>              
          </tr>
        </thead>
        <tbody>

          <?php
          $no=1;
          $select=mysqli_query($koneksi, "SELECT * from petugas ");
          while($data=mysqli_fetch_array($select)){
            ?>
            <tr>
              <td height="42"><?php echo $no++; ?></td>
              <td><?php echo $data['username']; ?></td>
              <td><?php echo $data['email'] ?></td>
              <td><?php echo $data['nama_petugas']?></td>                       
              <td>
                <a class="edit" href="edit_petugas_admin.php?id_petugas=<?php echo $data['id_petugas']; ?>">
                  <button type="button" class="btn btn-success" data-placement="left" title="Edit Petugas">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                  </button></a>
                  <a href='hapus_petugas_admin.php?id_petugas=<?=$data['id_petugas'];?>'><button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Hapus Petugas">
                     <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                  </button></a>              
                </td>          
           </tr>

           <?php
         }
         ?>
       </tbody>
     </table>
   </div>
 </div>
</div>
</div>
<?php
include 'footer.php';
?>
<!-- Datatables -->
<script src="assets/js/jquery.min.js"></script>
<script src="js/datatables/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function() {
    $('#riska').DataTable();
  });
</script>