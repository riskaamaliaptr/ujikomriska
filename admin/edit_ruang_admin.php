<?php
include '../koneksi.php';
include 'header.php';
$id_ruang=$_GET['id_ruang'] ;
$nama= mysqli_query($koneksi, "SELECT * FROM ruang where id_ruang='$id_ruang'");
$r = mysqli_fetch_array($nama);
?>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>         
        </nav>
    </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                <div class="row x_title">
                    <div class="col-md-6">
                        <h3>Edit Ruangan <small>Smkn 1 Ciomas</small></h3>
                    </div>                              
                </div>                            
            </div>
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <form action="" method="POST" class="form-horizontal form-label-left" novalidate>     
                        <span class="section">Ruangan</span>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_ruang">Nama Ruang <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="nama_ruang" name="nama_ruang" class="form-control col-md-7 col-xs-12" placeholder="nama_ruang" autocomplete="off" required="required" value="<?=$r['nama_ruang'];?>" readonly>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode_ruang">Kode Ruang <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="kode_ruang" name="kode_ruang" class="form-control col-md-7 col-xs-12" placeholder="kode_ruang" autocomplete="off" required="required" value="<?=$r['kode_ruang'];?>" readonly>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12" placeholder="keterangan" autocomplete="off" required="required"  value="<?=$r['keterangan'];?>">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="submit" name="submit" class="btn btn-info" value="Simpan" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
    include 'footer.php';
    ?>

    <?php
    include '../koneksi.php';
    if(isset($_POST['submit']))
    {
        $nama_ruang = $_POST['nama_ruang'];
        $kode_ruang = $_POST['kode_ruang'];
        $keterangan = $_POST['keterangan'];

        $edit = mysqli_query($koneksi, "UPDATE ruang SET nama_ruang='$nama_ruang',kode_ruang='$kode_ruang',keterangan='$keterangan' WHERE id_ruang='$_GET[id_ruang]'");
        if($edit){
          /*echo "<script>window.location.assign('ruang_admin.php')</script>";*/
          echo "<script>
          window.alert('Data Berhasil Di Edit')
          window.location.assign('ruang_admin.php')
          </script>";
      }else{
          echo"GAGAL";
      }
  }
  ?>