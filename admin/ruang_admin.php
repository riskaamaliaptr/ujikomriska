<?php 
include '../koneksi.php';
include 'header.php';
?>
<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav class="" role="navigation">
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div> 
    </nav>
  </div>
</div>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">
        <div class="row x_title">
          <div class="col-md-6">
            <h3>Data Ruangan <small>Smkn 1 Ciomas</small></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br />

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">  
      <button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Tambah Ruang
      </button>
      <a href="export_excel_ruang.php"> <button type="button" class="btn btn-primary">Export Excel</button></a>
      <a href="pdf_ruang.php"> <button type="button" class="btn btn-primary">Export Pdf</button></a>


      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
             <h4 class="modal-title" id="myModalLabel" align="center">Tambah Ruangan</h4>
           </div>
           <div class="modal-body">
            <form method="post"  action="pro_simpan_ruang_admin.php" class="form-group">
              <div class="container">
               <div class="col-sm-offset-1 col-sm-10">
                <div class="form-group">
                  <label>Nama Ruang</label>
                  <td><input type="text"  class="form-control"  name="nama_ruang" placeholder="Masukan Nama Ruang" autocomplete="off" required></td>
                </div>
                <div class="form-group">
                  <label>Kode Ruang</label>
                  <?php
                  $koneksi = mysqli_connect("localhost","root","","ujikom");
                  $cari_kd = mysqli_query($koneksi, "SELECT max(kode_ruang) as kode from ruang");
                                        // besar / kode yang masuk
                  $tm_cari = mysqli_fetch_array($cari_kd);
                  $kode = substr($tm_cari['kode'],1,4);
                  $tambah = $kode+1;
                  if ($tambah<10) {
                    $kode_ruang = "J000".$tambah;
                  }else{
                    $kode_ruang = "K00".$tambah;
                  }
                  ?>
                  <input class="form-control" name="kode_ruang" value="<?php echo $kode_ruang; ?>" type="text" placeholder="Masukan Kode Ruang" required readonly>
                </div>
                <div class="form-group">
                 <label>Keterangan</label>
                 <td><input type="text"  class="form-control"  name="keterangan" placeholder="Masukan Keterangan" autocomplete="off" required></td>
               </div>
             </div>
           </div>
         </div>
         <div class="modal-footer">
          <input type="button" class="btn btn-success" data-dismiss="modal" value="close">
          <input type="submit" class="btn btn-primary" name="simpan" value="simpan">
        </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>


<div class="row">
 <div class="col-md-12 col-sm-12 col-xs-12">
  <!-- !!!!!!!!!!!!Tabel!!!!!!!!! -->
  <div class="x_content">
    <div class="table-responsive">
      <table id="riska" class="table table-striped responsive-utilities jambo_table">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Ruang</th>
            <th>Kode Ruang</th>                  
            <th>keterangan</th>               
            <th>option</th>
          </tr>
        </thead>
        <tbody>

          <?php
          $no=1;
          $select=mysqli_query($koneksi, "SELECT * FROM ruang ");
          while($data=mysqli_fetch_array($select)){
            ?>
            <tr>
              <td height="42"><?php echo $no++; ?></td>
              <td><?php echo $data['nama_ruang']; ?></td>
              <td><?php echo $data['kode_ruang'] ?></td>                    
              <td><?php echo $data['keterangan']?></td>                       
              <td>
                <a href='edit_ruang_admin.php?id_ruang=<?=$data['id_ruang'];?>'><button type="button" class="btn btn-success" data-placement="left" title="Edit Ruang">
                  <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                </button></a>
                <a href='hapus_ruang_admin.php?id_ruang=<?=$data['id_ruang'];?>'><button type="button" class="btn btn-primary"  data-placement="left" title="Hapus Ruangan">
                 <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button></a>      
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

<?php
include 'footer.php';
?>
<!-- Datatables -->
<script src="assets/js/jquery.min.js"></script>
<script src="js/datatables/js/jquery.dataTables.min.js"></script>
<script src="js/assets/css/jquery.dataTables.min.css"></script>
<script>
  $(document).ready(function() {
    $('#riska').DataTable();
  });
</script>