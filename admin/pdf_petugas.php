<?php
include '../koneksi.php';
require('pdf/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'SMK NEGERI 1 CIOMAS',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'NSS:401020229101  NPSN:20254135',0,'C');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. Raya Laladon  Ds. Laladon Kec. Ciomas Kab. Bogor (0251) 7520933',0,'C');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Email : smkn_ciomas@yahoo.co.id Website : www.smkn1ciomas.sch.id ',0,'C');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Petugas",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(2, 0.8, 'No', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Username', 1, 0, 'C');
$pdf->Cell(7, 0.8, 'Email', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama Petugas', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($koneksi, "SELECT * FROM petugas");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(2, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['username'],1, 0, 'C');
	$pdf->Cell(7, 0.8, $lihat['email'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['nama_petugas'], 1, 1,'C');

	
	$no++;
}

$pdf->Output("laporan_ruang.pdf","I");

?>

