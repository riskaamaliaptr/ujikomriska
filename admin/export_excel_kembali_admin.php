<!DOCTYPE html>
<html>
<head>
  <title>Inventaris SMK</title>
</head>
<body>
  <style type="text/css">
  body{
    font-family: sans-serif;
  }
  table{
    margin: 20px auto;
    border-collapse: collapse;
  }
  table th,
  table td{
    border: 1px solid #3c3c3c;
    padding: 3px 8px;

  }
  a{
    background: blue;
    color: #fff;
    padding: 8px 10px;
    text-decoration: none;
    border-radius: 2px;
  }
</style>

<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Data Pengembalian Admin.xls");
?>

<center>
  <h1>Data Pengembalian Admin&operator </h1>
</center>

<table border="1">
 
  <thead>
    <tr>
      <th>No</th>
      <th>Nama Peminjam</th>
      <th>Nama Barang</th>                  
      <th>Tanggal Pinjam</th>
      <th>Tanggal Kembali</th>
      <th>Jumlah</th>                  
      <th>Status Peminjaman</th> 
      
    </tr>
  </thead>
  <tbody>

    <?php
    include '../koneksi.php';
    $no=1;
    $select=mysqli_query($koneksi, "SELECT * from peminjaman p JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman JOIN petugas g ON P.id_petugas=g.id_petugas JOIN inventaris i ON d.id_inventaris=i.id_inventaris  WHERE status_peminjaman='Telah Dikembalikan'");
    while($data=mysqli_fetch_array($select)){
      ?>
      <tr>                                
        <td height="42"><?php echo $no++; ?></td>
        <td><?php echo $data['username']; ?></td>
        <td><?php echo $data['nama']; ?></td>                    
        <td><?php echo $data['tanggal_pinjam']; ?></td> 
        <td><?php echo $data['tanggal_kembali']; ?></td> 
        <td><?php echo $data['jumlah_pinjam']; ?></td>                    
        <td><?php echo $data['status_peminjaman']; ?></td>                                        
      </tr>

      <?php
    }
    ?>
  </tbody>
</table>
</body>
</html>