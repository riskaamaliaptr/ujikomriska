<!DOCTYPE html>
<?php
  session_start();
  $username = $_SESSION['username'];
  include "../koneksi.php";
  if (isset($_SESSION['petugas'])){
?> 
<html>
<head>
    <title>Admin!</title>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="assets/css/jquery.dataTables.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
    <style>
    .modal-header {
        background: gainsboro;
    }
    .modal-footer .btn+.btn {
        margin-bottom: 6px;
    }
    </style>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a class="site_title"><i class="fa fa-paw"></i> <span>Inventaris!</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/admin3.gif" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>    
                        </div>
                    </div>
                    <!-- /menu prile quick info -->
                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>&nbsp;&nbsp;&nbsp;Admin!</h3>
                            <ul class="nav side-menu">
                                <!-- <li><a href="index_admin.php"><i class="fa fa-home"></i>Home</a></li> -->
                                <li><a><i class="fa fa-bar-chart-o"></i> Data Master <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="inventaris_admin.php">Inventaris</a>
                                        </li>
                                        <li><a href="jenis_admin.php">Jenis</a>
                                        </li>
                                        <li><a href="ruang_admin.php">Ruang</a>
                                        </li>
                                        <li><a href="datapetugas_admin.php">Data Petugas</a>
                                        </li>
                                        <li><a href="datapegawai_admin.php">Data Pegawai</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="peminjaman_admin.php"><i class="fa fa-edit"></i>Pinjam Barang</a></li>
                                <li><a><i class="fa fa-bookmark"></i> Data Admin&Operator <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="tbl_datapeminjaman_admin.php">Data Peminjaman</a>
                                        </li>  
                                        <li><a href="tbl_datapengembalian_admin.php">Data Pengembalian</a>
                                        </li>                                 
                                    </ul>
                                </li>

                                <li><a><i class="fa fa-bookmark"></i> Data User <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="tbl_datapeminjaman_user.php">Data Peminjaman</a>
                                        </li>
                                        <li><a href="tbl_datapengembalian_user.php">Data Pengembalian</a>
                                        </li>                  
                                    </ul>
                                </li>
                                
                                <li><a><i class="fa fa-print"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="backupdatabase.php">Backup Database</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="logout.php"><i class="fa fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>
        </body>
</html>
<?php
}else
    header("location:../login.php");
?>