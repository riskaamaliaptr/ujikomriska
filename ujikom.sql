-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2019 at 05:24 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` char(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_peminjaman`, `id_inventaris`, `jumlah_pinjam`) VALUES
(28, 34, 25, '1'),
(29, 35, 25, '1'),
(30, 36, 26, '1'),
(31, 37, 27, '1'),
(32, 38, 27, '1'),
(33, 39, 25, '1'),
(34, 40, 38, '1');

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `kondisi` varchar(30) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` char(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`, `gambar`) VALUES
(25, 'Laptop Acer', 'Baik', 'Tersedia', 49, 18, '2019-04-05', 10, 'V0001', 1, 'acer.jpg'),
(26, 'Laptop Lenovo', 'Baik', 'Tersedia', 49, 18, '2019-04-05', 10, 'V0002', 1, 'lenovo.jpg'),
(27, 'Mouse', 'Baik', 'Tersedia', 29, 18, '2019-04-05', 10, 'V0003', 1, 'mouse.png'),
(35, 'AirPhone', 'Baik', 'Tersedia', 25, 18, '2019-04-07', 10, 'V0004', 1, 'airphone.jpg'),
(36, 'Speaker', 'Baik', 'Tersedia', 10, 19, '2019-04-07', 16, 'V0005', 1, 'speaker.jpg'),
(38, 'Kunci Inggris', 'Baik', 'Tersedia', 9, 20, '2019-04-07', 17, 'V0006', 1, 'kunci_inggris.jpg'),
(39, 'Obeng', 'Baik', 'Tersedia', 10, 20, '2019-04-07', 17, 'V0007', 1, 'obeng.jpg'),
(40, 'Kunci L', 'Baik', 'Tersedia', 10, 20, '2019-04-07', 17, 'V0008', 1, 'kunci_l.jpg'),
(41, 'Kunci Ring', 'Baik', 'Tersedia', 10, 20, '2019-04-07', 17, 'V0009', 1, 'kunci_ring.jpg'),
(42, 'Helm Las', 'Baik', 'Tersedia', 5, 21, '2019-04-07', 18, 'V0010', 1, 'helm_las.jpg'),
(43, 'Kabel Las', 'Baik', 'Tersedia', 10, 21, '2019-04-07', 18, 'V0011', 1, 'kabel_las.jpg'),
(44, 'Mesin Las', 'Baik', 'Tersedia', 5, 21, '2019-04-07', 18, 'V0012', 1, 'mesin_las.jpg'),
(45, 'Kamera', 'Baik', 'Tersedia', 5, 22, '2019-04-07', 19, 'V0013', 1, 'kamera.jpg'),
(46, 'Kamera Tripod', 'Baik', 'Tersedia', 5, 22, '2019-04-07', 19, 'V0014', 1, 'tripod.jpg'),
(47, 'Microphone&Headphone', 'Baik', 'Tersedia', 5, 22, '2019-04-07', 19, 'V0015', 1, 'mic_hd.jpg'),
(48, 'Infocus', 'Baik', 'Tersedia', 15, 23, '2019-04-07', 20, 'V0016', 1, 'infocus.jpg'),
(49, 'Kabel Terminal', 'Baik', 'Tersedia', 10, 23, '2019-04-07', 20, 'V0017', 1, 'terminal.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` char(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(18, 'Rekayasa Perangkat Lunak', 'K0001', 'Ada'),
(19, 'Animasi', 'K0002', 'Ada'),
(20, 'Teknik Kendaraan Ringan', 'K0003', 'Ada'),
(21, 'Teknik Pengelasan', 'K0004', 'Ada'),
(22, 'Broadcasting', 'K0005', 'Ada'),
(23, 'Tata Usaha', 'K0006', 'Ada');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(30) NOT NULL,
  `nip` char(16) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `no_telp` char(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`, `no_telp`) VALUES
(1, 'riska', '12345', 'Dramaga', '089677948939'),
(2, 'amalia', '1234', 'Ciomas', '089677948939'),
(3, 'riskaamalia', '123456', 'Bogor', '089677948939');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(30) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `id_inventaris`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`, `id_petugas`) VALUES
(34, 25, '2019-04-05 11:33:26', '2019-04-07 21:23:59', 'Telah Dikembalikan', 1, 0),
(35, 25, '2019-04-05 11:42:19', '2019-04-06 14:44:44', 'Telah Dikembalikan', 0, 1),
(36, 26, '2019-04-05 11:53:23', '0000-00-00 00:00:00', 'sedang dipinjam', 0, 2),
(37, 27, '2019-04-07 07:30:18', '2019-04-07 22:11:44', 'Telah Dikembalikan', 0, 1),
(38, 27, '2019-04-07 10:35:42', '0000-00-00 00:00:00', 'sedang dipinjam', 0, 11),
(39, 25, '2019-04-07 10:49:33', '0000-00-00 00:00:00', 'sedang dipinjam', 0, 1),
(40, 38, '2019-04-07 12:00:43', '0000-00-00 00:00:00', 'sedang dipinjam', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `user_key` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `email`, `password`, `nama_petugas`, `id_level`, `status`, `user_key`) VALUES
(1, 'admin', 'ra2809507@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'riskaap', 1, 1, ''),
(11, 'operator', 'riskaamaliap97@gmail.com', '4b583376b2767b923c3e1da60d10de59', 'riskaamalia', 2, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(30) NOT NULL,
  `kode_ruang` char(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(9, 'Lab Rpl 1', 'J0001', 'Ada'),
(10, 'Lab Rpl 2', 'J0002', 'Ada'),
(11, 'Lab Rpl 3', 'J0003', 'Belum ada'),
(16, 'Studio Animasi', 'J0004', 'Ada'),
(17, 'Bengkel Tkr', 'J0005', 'Ada'),
(18, 'Bengkel Tpl', 'J0006', 'Ada'),
(19, 'Lab Broadcasting', 'J0007', 'Sedang Proses'),
(20, 'Tata Usaha', 'J0008', 'Ada');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_ruang` (`id_ruang`,`id_petugas`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
