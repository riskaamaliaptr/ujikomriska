<?php
session_start();
include 'koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Halaman Login!  </title>

    <!-- Bootstrap core CSS -->
    <link href="admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="admin/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="admin/css/animate.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="admin/css/custom.css" rel="stylesheet">
    <link href="admin/css/icheck/flat/green.css" rel="stylesheet">
    <script src="admin/js/jquery.min.js"></script>
</head>

<body background="admin/images/1.jpg">   
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>
        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <form action="" method="post">
                        <h1>Login Pegawai</h1>
                        <div>
                            <input type="text" class="form-control" name="nip" placeholder="nip" autocomplete="off"  />
                        </div>      
                        <div>                        
                            <button type="submit" name="submit">Login</button>
                        </div>             
                        <div class="clearfix"></div>
                        <div class="separator">
                            <div>
                                <h1><i class="fa fa-home" style="font-size: 26px;"></i> Inventaris Sekolah</h1>
                                <p>©2019 Jl.Raya Laladon Ds.Laladon Kec.Ciomas Kab.Bogor  </p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>

        <?php
            @session_start();
            //menghubungkan php dengan DB
            if (isset($_POST['submit'])) {
                include 'koneksi.php';

                $nip = $_POST['nip'];

                //menyeleksi data user yang sesuai
                $login = mysqli_query($koneksi, "SELECT * FROM pegawai WHERE nip='$nip'");

                //menghitung jumlah data yang ditentukan
                $cek = mysqli_num_rows($login);

                if ($cek > 0) {
                    $data = mysqli_fetch_assoc($login);
                
                //cek jika user login sebagai admin

                //buat session login dan username
                $_SESSION['pegawai']=$data['id_pegawai'];
                $_SESSION['nama_pegawai']=$data['nama_pegawai'];
                $_SESSION['level']= "pegawai";

                echo "<script>
                    window.alert('Welcome To Pengguna')
                    window.location='user/inventaris_user.php'
                </script>";
            }else{
                echo "<script>
            window.alert('Maaf anda gagal login')
            window.location='login_user.php'
        </script>";
            }
        }
        ?>
    </div>
</body>
</html>