<!DOCTYPE html>
<html>
<head>
	<title>Help</title>
</head>
<body>
	<div class="container">
		<img src="gambar_barang/table.png">
		<bold><h3>&nbsp;&nbsp;&nbsp;Sebagai Admin dan Operator</h3></bold>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> Untuk masuk ke halaman login sebagai Admin dan Operator ketikan http://ujikomriska.sknc.site/ujikom_riskaa/ujikom_riskaa/login.php <br> &nbsp;&nbsp;&nbsp;&nbsp; kemudian link tersebut akan menuju ke halaman login.</p>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> Admin/Operator melakukan login dengan cara memasukan username dan password, kemudian menekan tombol login.</p>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> Pada halaman Admin, ada data inventaris yang dapat dilihat sesuai jurusan. Pada table inventaris terdapat data jenis, data ruangan, data pegawai, data petugas yag sudah ada.</p>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> peminjaman barang pada halaman admin dan operator, barang dapat dipinjam sesuai jurusan. Jika ingin meminjam laptop maka kita bisa memilih menu jurusan RPL yang didalamnya &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;terdapat form yang harus diisi. Dan data yang sudah dipinjam akan masuk kedalam fitur Data Peminjaman. Kemudian jumlah barang inventaris akan berkurang setelah dipinjam dan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;akan kembali jika sudah dikembalikan. </p>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> Jika barang ingin dikembalikan maka harus konfirmasi ke admin atau operator. Maka dapat dilihat pada tabel data peminjaman kemudian klik button kembalikan.</p>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> Jika ingin melihat data yang sudah mengembalikan barang, Maka pilih fitur data pengembalian  .</p>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> Admin atau Operator dapat keluar dari halaman website dengan cara memilih menu keluar (logout).</p><br>

		<bold><h3>&nbsp;&nbsp;&nbsp;Sebagai User</h3></bold>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> User melakukan pendaftaran sebagai member baru dengan cara mendatangi admin untuk mendapatkan password(Nip)..</p>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> Untuk masuk ke halaman login sebagai User ketikan http://ujikomriska.sknc.site/ujikom_riskaa/ujikom_riskaa/login_user.php <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kemudian link tersebut akan menuju ke halaman login user.</p>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> User melakukan login dengan cara memasukan Nip, kemudian menekan tombol login.</p>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> peminjaman barang pada halaman User , barang dapat dipinjam sesuai jurusan. Jika ingin meminjam laptop maka kita bisa memilih menu jurusan RPL yang didalamnya terdapat form &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;yang harus diisi. Dan data yang sudah dipinjam akan masuk kedalam fitur Data Peminjaman yang berada di halaman ADMIN.</p>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> Jika barang ingin dikembalikan maka harus konfirmasi ke admin atau operator. Maka dapat dilihat pada tabel data peminjaman yang berada pada halaman ADMIN.</p>
		<p><bold>&nbsp;&nbsp;&nbsp;*</bold> User dapat keluar dari halaman website dengan cara memilih menu keluar (logout).. </p>
	</div>
</table>
</body>
</html>